#include <cstdio>

#include <xtl/ini/IniConfig.hpp>
#include <xtl/linux/UnixError.hpp>

/*
class IniConfigTester
{
	public:

		explicit IniConfigTester(const XTL::IniConfig & config)
			: config_(config)
		{
			;;
		}

		void Test(const std::string & section, const std::string & key, const std::string & value) const
		{
			if (config_.GetSectionRequired(section).GetString(key, "") != value)
			{
				throw IniConfig::Error("");
			}
		}

	private:

		const XTL::IniConfig & config_;
};
*/

int main(int argc, char * argv[])
{
	XTL::IniConfig config;

	try
	{
		config.LoadFromFile("../tests/data/main.ini");
		long long int v = config.GetSectionRequired("my_location").GetLongLongInt("name");
		fprintf(stderr, "%lld\n", v);
		fprintf(stderr, "%s\n", config.GetSectionRequired("my_location").GetString("key5").c_str());
		fprintf(stderr, "%s\n", config.GetSectionRequired("my_location").GetString("z", "").c_str());
	}
	catch (const XTL::IniConfig::Error & e)
	{
		fprintf(stderr, "%s\n", e.What().c_str());
	}
	catch (const XTL::UnixError & e)
	{
		fprintf(stderr, "%s\n", e.What().c_str());
	}
}

