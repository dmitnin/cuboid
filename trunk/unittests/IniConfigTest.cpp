#include "IniConfigTest.hpp"

#include <string>

#include <xtl/FormatString.hpp>
#include <xtl/StringUtils.hpp>
#include <xtl/Types.hpp>
#include <xtl/linux/UnixError.hpp>
#include <xtl/linux/fs/File.hpp>
#include <xtl/linux/fs/FileStats.hpp>
#include <xtl/linux/fs/FileUtils.hpp>
#include <xtl/linux/utils/Execute.hpp>
#include <xtl/ini/IniConfig.hpp>

CPPUNIT_TEST_SUITE_REGISTRATION (IniConfigTest);

namespace
{
	void PrepareFile(const std::string & filePath, const std::string & content)
	{
		try
		{
			const XTL::FileType fileType = XTL::FileStats::Type(filePath, true);

			if (fileType == XTL::FileType::REGULAR)
			{
				const XTL::FileStats fileStats(filePath, true);
				if (fileStats.Size() == static_cast<int>(content.size()))
				{
					return;
				}
			}
			else if (fileType != XTL::FileType::NOT_EXISTS)
			{
				CPPUNIT_FAIL(XTL::FormatString("Could not create file '%s' because it already exists", filePath));
			}
		}
		catch (const XTL::UnixError & e)
		{
			CPPUNIT_FAIL(XTL::FormatString("Could not check file '%s': %s", filePath, e.What()));
		}

		try
		{
			XTL::FileUtils::CreatePathForFile(filePath);
			XTL::File file(filePath);
			file.Create(XTL::File::RECREATE_READ_WRITE);
			file.Write(content.data(), content.size());
			file.Close();
		}
		catch (const XTL::UnixError & e)
		{
			CPPUNIT_FAIL(XTL::FormatString("Could not create path to file '%s': %s", filePath, e.What()));
		}
		catch (...)
		{
			fprintf(stderr, "Exception\n");
		}
	}
}

const std::string & ConfigBaseDir()
{
	static const std::string s("/tmp/iniconfig_test/");
	return s;
}

void IniConfigTest::setUp()
{
	PrepareFile(ConfigBaseDir() + "first/included.cfg",
"\
{set database_config_name = 'dbconfig'}\n\
{include \"second/${database_config_name}.cfg\"}\n\
");

	PrepareFile(ConfigBaseDir() + "first/second/dbconfig.cfg",
"\
[Database]\n\
user = \"lj\"\n\
password = \"test\"\n\
");

	PrepareFile(ConfigBaseDir() + "types.cfg",
"# Comments line\n\
; Another comments line\n\
[ Values ]\n\
the_integer1 = 0\n\
the_integer2 = -0\n\
the_integer3 = 65536\n\
the_integer4 = 0b00001111\n\
the_integer5 = 0xdeadbeef\n\
the_empty_string1 : ''\n\
the_empty_string2 \"\"\n\
the_number1 = 0.0\n\
the_number2 = -3.1415926\n\
the_number3 = 1024.535e-3\n\
the_number4 = -1e+6\n\
the_string_like_int = \"105\"\n\
the_string_like_number = '10.01e+2'\n\
\n\
without_spaces=\"value2\"\n\
\n\
{set d='/'}\n\
{set root=\"/home${d}lj\"}\n\
\n\
key1 = '${root}'\n\
key2 = \"${root\"\n\
key3 = \"${root}\"\n\
key4 : \"${root}/cgi-bin\"\n\
key5 = \"${}\"\n\
key6 = \"${  }\"\n\
key7 = \"${ spaces }\"\n\
key8 = \"${spaces }\"\n\
\n\
{include \"./first/included.cfg\"}\n\
");
}

namespace
{
	class ErrorListener : public XTL::ForkExecErrorListener
	{
		public:

			virtual ~ErrorListener() throw() { ;; }

			virtual void OnExecError(const std::string & filePath, const XTL::UnixError & e) const
			{
				fprintf(stderr, "\n*** Could not run '%s': %s\n", filePath.c_str(), e.What().c_str());
			}

			virtual void OnDoubleForkError(const XTL::UnixError & e) const { ;; }
	};
}

void IniConfigTest::tearDown()
{
	std::vector<std::string> arguments;
	arguments.push_back("-rf");
	arguments.push_back(ConfigBaseDir());
	ForkExecWait("rm", arguments, ErrorListener());
}

void IniConfigTest::MainTest()
{
	XTL::IniConfig config;

	try
	{
		config.LoadFromFile(ConfigBaseDir() + "types.cfg");
		const XTL::IniConfig::Section & section = config.GetSectionRequired("Values");
		CPPUNIT_ASSERT_EQUAL(section.GetInt("the_integer1"), 0);
		CPPUNIT_ASSERT_EQUAL(section.GetInt("the_integer2"), 0);
		CPPUNIT_ASSERT_EQUAL(section.GetInt("the_integer3"), 65536);
		CPPUNIT_ASSERT_EQUAL(section.GetInt("the_integer4"), 15);
		CPPUNIT_ASSERT_EQUAL(section.GetLongLongInt("the_integer5"), 3735928559ll);
		CPPUNIT_ASSERT_EQUAL(section.GetString("the_empty_string1"), std::string(""));
		CPPUNIT_ASSERT_EQUAL(section.GetString("the_empty_string2"), std::string(""));
		CPPUNIT_ASSERT_EQUAL(section.GetDouble("the_number1"), 0.0);
		CPPUNIT_ASSERT_EQUAL(section.GetDouble("the_number2"), -3.1415926);
		CPPUNIT_ASSERT_EQUAL(section.GetDouble("the_number3"), 1024.535e-3);
		CPPUNIT_ASSERT_EQUAL(section.GetLongLongInt("the_number4"), -1000000ll);
		CPPUNIT_ASSERT_EQUAL(section.GetInt("the_string_like_int"), 105);
		CPPUNIT_ASSERT_EQUAL(section.GetDouble("the_string_like_number"), 1001.0);
		CPPUNIT_ASSERT_EQUAL(section.GetString("without_spaces"), std::string("value2"));

		CPPUNIT_ASSERT_EQUAL(section.GetString("key1"), std::string("${root}"));
		CPPUNIT_ASSERT_EQUAL(section.GetString("key2"), std::string("${root"));
		CPPUNIT_ASSERT_EQUAL(section.GetString("key3"), std::string("/home/lj"));
		CPPUNIT_ASSERT_EQUAL(section.GetString("key4"), std::string("/home/lj/cgi-bin"));

		CPPUNIT_ASSERT_EQUAL(section.GetString("key5"), std::string("${}"));
		CPPUNIT_ASSERT_EQUAL(section.GetString("key6"), std::string("${  }"));
		CPPUNIT_ASSERT_EQUAL(section.GetString("key7"), std::string("${ spaces }"));
		CPPUNIT_ASSERT_EQUAL(section.GetString("key8"), std::string("${spaces }"));

		const XTL::IniConfig::Section & sectionDatabase = config.GetSectionRequired("Database");
		CPPUNIT_ASSERT_EQUAL(sectionDatabase.GetString("user"), std::string("lj"));
		CPPUNIT_ASSERT_EQUAL(sectionDatabase.GetString("password"), std::string("test"));
	}
	catch (const XTL::IniConfig::Error & e)
	{
		CPPUNIT_FAIL(XTL::FormatString("XTL::IniConfig::Error was caught: %s", e.What()));
	}
	catch (const XTL::UnixError & e)
	{
		CPPUNIT_FAIL(XTL::FormatString("XTL::UnixError was caught: %s", e.What()));
	}
}
