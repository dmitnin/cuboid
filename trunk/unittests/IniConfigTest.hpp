#ifndef XTL__INI_CONFIG_TEST_HPP__
#define XTL__INI_CONFIG_TEST_HPP__ 1

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class IniConfigTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(IniConfigTest);
	CPPUNIT_TEST(MainTest);
	CPPUNIT_TEST_SUITE_END();

	public:

		void setUp();

		void tearDown();

	protected:

		void MainTest();
};

#endif

