#include "FilePathTest.hpp"

#include <string>

#include <xtl/linux/fs/FilePath.hpp>
#include <xtl/Types.hpp>

CPPUNIT_TEST_SUITE_REGISTRATION (FilePathTest);

std::ostream & operator<< (std::ostream & os, const XTL::FilePath & filePath)
{
	os << filePath.ToString();
	return os; 
}

void FilePathTest::Test1()
{
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("").IsAbsolute(), false);
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("").IsEmpty(), true);
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath(" ").IsEmpty(), false);
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("/").IsAbsolute(), true);
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("////").IsAbsolute(), true);
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("////").IsEmpty(), false);
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("").ToString(), std::string("."));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("//").ToString(), std::string("/"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("/../../..").ToString(), std::string("/"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("//a/../b/"), XTL::FilePath("/b"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("/a/b/c/d").Parent(), XTL::FilePath("/a/b/c"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b/c/d").Parent(), XTL::FilePath("a/b/c"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b").Parent().Parent(), XTL::FilePath("."));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b").Parent().Parent().Parent(), XTL::FilePath(".."));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b").Parent().Parent().Parent().Parent(), XTL::FilePath("../.."));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("../../a/b").Parent().Parent().Parent().Parent(), XTL::FilePath("../../../.."));

	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b") + "c", XTL::FilePath("a/b/c"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b") + "c/d/e", XTL::FilePath("a/b/c/d/e"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b") + "c/../e", XTL::FilePath("a/b/e"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b") + "../c", XTL::FilePath("a/c"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b") + "/c/d", XTL::FilePath("a/b/c/d"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b") + "/../c/", XTL::FilePath("a/b/c"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b") + XTL::FilePath("/../c"), XTL::FilePath("a/b/c"));
	CPPUNIT_ASSERT_EQUAL(XTL::FilePath("a/b") + ".", XTL::FilePath("a/b"));
	CPPUNIT_ASSERT_EQUAL(".." + XTL::FilePath(".."), XTL::FilePath("../.."));
}
