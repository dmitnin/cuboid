#ifndef XTL__INI_CONFIG_HPP__
#define XTL__INI_CONFIG_HPP__ 1

#include <algorithm>
#include <list>
#include <map>
#include <set>
#include <stack>
#include <string>

#include "../Exception.hpp"
#include "../Types.hpp"
#include "../VariantPtr.hpp"
#include "../utils/AutoPtrMap.hpp"
#include <xtl/SharedPtr.hpp>
#include <xtl/linux/fs/FilePath.hpp>
#include <xtl/linux/fs/FileStats.hpp>

/*
; Comments string
[SectionName]
  Parameter1 = Value1  ; Comments
  Parameter2 : Value2  # Comments
  Parameter3   Value3

* Number
    -?1[0-9]* (\.[0-9]+)? ([eE][+-]?[0-9]+)?
    0b[01]+
    0x[0-9A-Fa-f]+
    0[0-7]+
* String - однострочный строковый литерал
    " ([^\]|\ESC)* "  ESC = ["\", '"', "r", "n", "t"]
    ' ([^\]|\ESC)* '  ESC = ["\", "'"]

{include "StringLiteral"}
{set Var = "StringLiteral"}
$Var
${Var}

  {set BinDir = "/usr/local/bin"}
  Param = "$BinDir/program_name"
*/

namespace XTL
{
	static const FileSize MAX_INI_CONFIG_SIZE = 128 * 1024 * 1024;

	class IniConfig
	{
		public:

			class Location
			{
				public:

					Location(SharedPtr<const FilePath> filePath, unsigned int fileLine)
						: filePath_(filePath),
						  fileLine_(fileLine)
					{
						;;
					}

					const std::string File() const
					{
						return filePath_->ToString();
					}

					const unsigned int Line() const
					{
						return fileLine_;
					}

				private:

					SharedPtr<const FilePath> filePath_;
					unsigned int              fileLine_;
			};

			struct LocatedValue
			{
				Location   location;
				VariantPtr value;

				LocatedValue(const Location & location_, const VariantPtr & value_)
					: location(location_),
					  value(value_)
				{
					;;
				}
			};

			class Error : public XTL::Exception
			{
				public:

					explicit Error(const std::string & what);

					virtual ~Error() throw();

					virtual const std::string What() const;

					class SectionDoesNotExist;
					class KeyDoesNotExist;

				private:

					const std::string what_;
			};

			class Context
			{
				typedef std::list<SharedPtr<const FilePath> > FilePathStack;

				public:

					explicit Context(const std::string & baseDirPath);

					void LoadFile(const std::string & filePathStr, IniConfig & config);

					SharedPtr<const FilePath> CurrentFilePath() const;

					void PushFilePath(SharedPtr<const FilePath> filePath);

					void PopFilePath();

					void SetVariable(const std::string & key, const Location & location, VariantPtr value);

					void AppendVariable(std::deque<char> & result, const std::string & key);

					/* Substitute values of all ${key}.*/
					void InterpolateString(std::string & s);

				private:

					SharedPtr<const FilePath> FindFile(const std::string & filePathStr) const;

					bool IsCircularIncluding(const FilePath & filePath) const;

					bool WasAlreadyIncluded(const FilePath & filePath) const;

					void LoadFile(SharedPtr<const FilePath> filePath, IniConfig & config);

					FilePath                            baseDirPath_;
					FilePathStack                       filePathStack_;
					std::set<std::string>               includedFiles_;
					std::map<std::string, LocatedValue> variables_;
			};

			class Section;

			IniConfig();

			void Set(const std::string & sectionName, const std::string & key, const Location & location, VariantPtr value);

			/**
			 * @throw XTL::IniConfig::Error::SectionDoesNotExist
			 */
			const Section & GetSectionRequired(const std::string & sectionName) const;

			const Section & GetSectionOptional(const std::string & sectionName) const;

			void LoadFromFile(const std::string & filePath);

		private:

			Section & CreateSection(const std::string & sectionName);

			AutoPtrMap<std::string, Section> sections_;
			std::auto_ptr<const Section> emptySection_;
	};


	class IniConfig::Section
	{
		public:

			explicit Section(const IniConfig & config, const std::string & sectionName);

			/**
			 * Метод для каскадного вызова GetSectionRequired и методов Fetch.
			 */
			const Section & GetSectionRequired(const std::string & sectionName) const;

			const Section & GetSectionOptional(const std::string & sectionName) const;

			bool Contains(const std::string & key) const;

			void Set(const std::string & key, const Location & location, const VariantPtr & value);

			/**
			 * @throw XTL::IniConfig::Error::KeyDoesNotExist
			 */
			VariantPtr Get(const std::string & key) const;

			const int GetInt(const std::string & key) const;

			const int GetInt(const std::string & key, int defaultValue) const;

			/**
			 * Если заданный ключ не найден, выбрасывается исключение.
			 * @throw XTL::IniConfig::Error::KeyDoesNotExist
			 */
			const long long int GetLongLongInt(const std::string & key) const;

			/**
			 * Если заданный ключ не найден, метод возвращает defaultValue
			 */
			const long long int GetLongLongInt(const std::string & key, const long long int & defaultValue) const;

			const double GetDouble(const std::string & key) const;

			const double GetDouble(const std::string & key, const double & defaultValue) const;

			const std::string GetString(const std::string & key) const;

			const std::string GetString(const std::string & key, const std::string & defaultValue) const;

			const Section & Fetch(const std::string & key, int & value) const;

			const Section & Fetch(const std::string & key, int & value, int defaultValue) const;

			const Section & Fetch(const std::string & key, unsigned int & value) const;

			const Section & Fetch(const std::string & key, unsigned int & value, unsigned int defaultValue) const;

			const Section & Fetch(const std::string & key, long long int & value) const;

			const Section & Fetch(const std::string & key, long long int & value, long long int defaultValue) const;

			const Section & Fetch(const std::string & key, double & value) const;

			const Section & Fetch(const std::string & key, double & value, double defaultValue) const;

			const Section & Fetch(const std::string & key, std::string & value) const;

			const Section & Fetch(const std::string & key, std::string & value, const std::string & defaultValue) const;

		private:

			const IniConfig & config_;
			const std::string sectionName_;

			mutable std::map<std::string, LocatedValue> pairs_;
	};


	class IniConfig::Error::SectionDoesNotExist : public IniConfig::Error
	{
		public:

			explicit SectionDoesNotExist(const std::string & sectionName);

			virtual ~SectionDoesNotExist() throw();

			const std::string & SectionName() const;

		private:

			const std::string sectionName_;
	};


	class IniConfig::Error::KeyDoesNotExist : public IniConfig::Error
	{
		public:

			KeyDoesNotExist(const std::string & sectionName, const std::string & key);

			virtual ~KeyDoesNotExist() throw();

			const std::string & SectionName() const;

			const std::string & Key() const;

		private:

			const std::string sectionName_;
			const std::string key_;
	};
}

#endif
