#include "IniConfigParser.hpp"

#include <cassert>
#include "../VariantScalar.hpp"
#include "../tp/NumberLiteralParser.hpp"
#include "../tp/StringLiteralParser.hpp"

namespace XTL
{
	IniConfigParser::IniConfigParser(CharSource & charSource, IniConfig & config, IniConfig::Context & configContext)
		: Parser(charSource),
		  config_(config),
		  configContext_(configContext),
		  currentSection_(),
		  currentLine_(1)
	{
		try
		{
			while (NotAtEnd())
			{
				ParseLine();
			}
		}
		catch (const Parser::Error & e)
		{
			throw IniConfig::Error(XTL::FormatString("%s at '%s' line %u column %u", e.What(), configContext_.CurrentFilePath()->ToString(), e.Cursor().Row() + 1, e.Cursor().Column()));
		}
	}

	const IniConfig::Location IniConfigParser::GetLocation() const
	{
		return IniConfig::Location(configContext_.CurrentFilePath(), currentLine_);
	}

	void IniConfigParser::ParseLine()
	{
		SkipLinearSpaces();
		if (AtEnd())
		{
			return;
		}

		char c = GetChar();
		if (c == '[')
		{
			ReadSection();
		}
		else if (CharClass::IDENTIFIER_HEAD.Contains(c))
		{
			ReadKeyValue();
		}
		else if (c == '{')
		{
			ReadCommand();
		}

		SkipLinearSpaces();
		if (AtEnd())
		{
			return;
		}

		if (GetChar() == ';' || GetChar() == '#')
		{
			SkipComments();
		}

		SkipNewLine();
	}

	void IniConfigParser::ReadSection()
	{
		assert(NotAtEnd() && GetChar() == '[');

		try
		{
			Advance();
			SkipLinearSpaces();
			if (!CharClass::IDENTIFIER_HEAD.Contains(NeedChar()))
			{
				ThrowError("Section name was expected");
			}
			currentSection_ = ReadIdentifier();
			SkipLinearSpaces();
			if (NeedChar() != ']')
			{
				ThrowError("Closing square bracket was expected");
			}
			Advance();
		}
		catch (const EndOfFile &)
		{
			ThrowError("Unexpected end of file");
		}
	}

	void IniConfigParser::ReadKeyValue()
	{
		assert(NotAtEnd() && CharClass::IDENTIFIER_HEAD.Contains(GetChar()));

		const std::string key = ReadIdentifier();

		bool wasDivider = false;
		if (NotAtEnd() && InClass(CharClass::LINEAR_SPACE))
		{
			wasDivider = true;
			Advance();
			SkipLinearSpaces();
		}

		if (NotAtEnd())
		{
			char c = GetChar();
			if (c == ':' || c == '=')
			{
				wasDivider = true;
				Advance();
				SkipLinearSpaces();
			}
		}

		if (!wasDivider)
		{
			ThrowError("A divider of key-value was expected (':', '=', SPACE or TAB)");
		}

		if (AtEnd())
		{
			ThrowError("Value expected");
		}

		config_.Set(currentSection_, key, GetLocation(), ReadValue());
	}

	namespace
	{
		class SingleQuotedStringParser : public XTL::StringLiteralParser
		{
			public:

				explicit SingleQuotedStringParser(XTL::CharSource & charSource)
					: StringLiteralParser(charSource, '\'', '\\', false)
				{
					;;
				}

				virtual ~SingleQuotedStringParser() throw() { ;; }

			protected:

				class ES : public StringLiteralParser::EscapeSequenceSet<ES>
				{
					public:

						ES()
							: XTL::StringLiteralParser::EscapeSequenceSet<ES>()
						{
							Add('\'', '\'');
							Add('\\', '\\');
						}
				};

				virtual void ParseEscapeSequence(XTL::CharBuffer & result)
				{
					ES::Instance().Parse(GetCharSource(), result);
				}
		};

		class DoubleQuotedStringParser : public XTL::StringLiteralParser
		{
			public:

				explicit DoubleQuotedStringParser(XTL::CharSource & charSource)
					: XTL::StringLiteralParser(charSource, '"', '\\', false)
				{
					;;
				}

				virtual ~DoubleQuotedStringParser() throw() { ;; }

			protected:

				class ES : public XTL::StringLiteralParser::EscapeSequenceSet<ES>
				{
					public:

						ES()
							: XTL::StringLiteralParser::EscapeSequenceSet<ES>()
						{
							Add('"', '"');
							Add('\\', '\\');
							Add('r', '\r');
							Add('n', '\n');
							Add('t', '\t');
						}
				};

				virtual void ParseEscapeSequence(XTL::CharBuffer & result)
				{
					ES::Instance().Parse(GetCharSource(), result);
				}
		};
	}

	/*
	 * " ( [^\\"] | '\\' [rnt\\"]  )* "
	 * ' ( [^\\'] | '\\' [\\'] )* '
	 * -? [0-9]+ ( '.' [0-9]+ )? ( [eE] [+-] [0-9]+ )
	 * '0' ( ('x' [0-9A-Fa-f]+) | ('b' [0-1]+) | ([0-7]*) )
	 */
	VariantPtr IniConfigParser::ReadValue()
	{
		assert(NotAtEnd() && NotInClass(CharClass::LINEAR_SPACE));

		char c = GetChar();

		if (c == '\'')
		{
			return VariantPtr(new Variant::String(SingleQuotedStringParser(GetCharSource()).Parse()));
		}
		else if (c == '"')
		{
			std::string value(DoubleQuotedStringParser(GetCharSource()).Parse());
			configContext_.InterpolateString(value);
			return VariantPtr(new Variant::String(value));
		}
		else if (CharClass::NUMBER_HEAD.Contains(c))
		{
			FloatLiteralParser numberParser(GetCharSource(), FloatLiteralParser::PARSE_BINARY |
			                                                 FloatLiteralParser::PARSE_OCTAL  |
			                                                 FloatLiteralParser::PARSE_HEXADECIMAL);
			const Number n = numberParser.Parse();

			if (n.IsRational())
			{
				return VariantPtr(new Variant::Double(n.ToDouble()));
			}
			else
			{
				return VariantPtr(new Variant::LongLongInt(n.ToSignedInteger()));
			}
		}
		/*
		else if (CharClass::IDENTIFIER_HEAD.Contains(c))
		{
		}
		*/
		else
		{
			ThrowError("Value was expected");

			// To avoid warning "control reaches end of non-void function"
			return Variant::Null();
		}
	}

	void IniConfigParser::SkipComments()
	{
		assert(NotAtEnd() && (GetChar() == ';' || GetChar() == '#'));

		do
		{
			Advance();
		}
		while (NotAtEnd() && GetChar() != '\n');
	}

	void IniConfigParser::SkipNewLine()
	{
		if (AtEnd())
		{
			return;
		}

		if (GetChar() == '\r')
		{
			Advance();
			if (AtEnd())
			{
				return;
			}
		}

		if (GetChar() == '\n')
		{
			++currentLine_;
			Advance();
		}
		else
		{
			ThrowError("New line expected");
		}
	}

	void IniConfigParser::ReadCommand()
	{
		assert(NotAtEnd() && GetChar() == '{');

		try
		{
			Advance();
			SkipLinearSpaces();
			if (!CharClass::IDENTIFIER_HEAD.Contains(NeedChar()))
			{
				ThrowError("Command was expected");
			}

			const std::string command = ReadIdentifier();

			if (!SkipChar(CharClass::LINEAR_SPACE))
			{
				ThrowError("Space character was expected");
			}

			SkipLinearSpaces();

			if (command == "include")
			{
				try
				{
					const std::string filePathStr = ReadValue().ToString();
					configContext_.LoadFile(filePathStr, config_);
				}
				catch (const IniConfig::Error & e)
				{
					ThrowError(e.What().c_str());
				}
			}
			else if (command == "set")
			{
				if (!CharClass::IDENTIFIER_HEAD.Contains(NeedChar()))
				{
					ThrowError("Identifier was expected");
				}

				const std::string varName = ReadIdentifier();

				SkipLinearSpaces();
				if (!SkipChar('='))
				{
					ThrowError("Char '=' was expected");
				}

				SkipLinearSpaces();

				configContext_.SetVariable(varName, GetLocation(), ReadValue());
			}
			else
			{
				ThrowError("Unknown command '%s'", command);
			}

			SkipLinearSpaces();

			if (NeedChar() != '}')
			{
				ThrowError("Closing curly bracket was expected");
			}
			Advance();
		}
		catch (const EndOfFile &)
		{
			ThrowError("Unexpected end of file");
		}
	}
}
