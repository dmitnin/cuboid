#include "IniConfig.hpp"

#include <set>
#include <vector>

#include "IniConfigParser.hpp"
#include "../FormatString.hpp"
#include "../Types.hpp"
#include "../VariantScalar.hpp"
#include <xtl/linux/UnixError.hpp>
#include <xtl/linux/fs/File.hpp>
#include <xtl/linux/fs/FilePath.hpp>
#include <xtl/linux/fs/FileUtils.hpp>

namespace XTL
{
	/********** IniConfig::Error:: **********/

	IniConfig::Error::Error(const std::string & what)
		: what_(what)
	{
		;;
	}

	IniConfig::Error::~Error() throw()
	{
		;;
	}

	const std::string IniConfig::Error::What() const
	{
		return what_;
	}

	/********** IniConfig::Error::SectionDoesNotExist **********/

	IniConfig::Error::SectionDoesNotExist::SectionDoesNotExist(const std::string & sectionName)
		: Error(FormatString("Section '%s' does not exist", sectionName)),
		  sectionName_(sectionName)
	{
		;;
	}

	IniConfig::Error::SectionDoesNotExist::~SectionDoesNotExist() throw()
	{
		;;
	}

	const std::string & IniConfig::Error::SectionDoesNotExist::SectionName() const
	{
		return sectionName_;
	}

	/********** IniConfig::Error::KeyDoesNotExist **********/

	IniConfig::Error::KeyDoesNotExist::KeyDoesNotExist(const std::string & sectionName, const std::string & key)
		: Error(FormatString("Section '%s' does not contain key '%s'", sectionName_, key_)),
		  sectionName_(sectionName),
		  key_(key)
	{
		;;
	}

	IniConfig::Error::KeyDoesNotExist::~KeyDoesNotExist() throw()
	{
		;;
	}

	const std::string & IniConfig::Error::KeyDoesNotExist::SectionName() const
	{
		return sectionName_;
	}

	const std::string & IniConfig::Error::KeyDoesNotExist::Key() const
	{
		return key_;
	}

	/********** IniConfig::Context **********/

	IniConfig::Context::Context(const std::string & baseDirPath)
		: baseDirPath_(baseDirPath),
		  filePathStack_(),
		  includedFiles_(),
		  variables_()
	{
		;;
	}

	SharedPtr<const FilePath> IniConfig::Context::FindFile(const std::string & filePathStr) const
	{
		SharedPtr<const FilePath> fileAbsolutePath;

		FilePath filePath(filePathStr);
		FileType fileType = FileType::UNKNOWN;

		if (filePath.IsAbsolute())
		{
			fileAbsolutePath.Reset(new FilePath(filePath));
			fileType = FileStats::Type(filePath.ToString(), true);
		}
		else
		{
			if (!filePathStack_.empty())
			{
				fileAbsolutePath.Reset(new FilePath(filePathStack_.back()->Parent() + filePath));
				fileType = FileStats::Type(fileAbsolutePath->ToString(), true);
			}

			if (!baseDirPath_.IsEmpty() && (fileType == FileType::UNKNOWN || fileType == FileType::NOT_EXISTS))
			{
				fileAbsolutePath.Reset(new FilePath(baseDirPath_ + filePath));
				fileType = FileStats::Type(fileAbsolutePath->ToString(), true);
			}
		}

		if (fileType != FileType::REGULAR)
		{
			throw Error(FormatString("Included file '%s' is not regular or does not exist", filePathStr));
		}

		return fileAbsolutePath;
	}

	namespace
	{
		struct PtrEquals
		{
			const FilePath & filePath_;

			explicit PtrEquals(const FilePath & filePath) : filePath_(filePath) { ;; }

			const bool operator() (SharedPtr<const FilePath> ptr) const
			{
				return *ptr == filePath_;
			}
		};
	}

	bool IniConfig::Context::IsCircularIncluding(const FilePath & filePath) const
	{
		return std::find_if(filePathStack_.begin(), filePathStack_.end(), PtrEquals(filePath)) != filePathStack_.end();
	}

	bool IniConfig::Context::WasAlreadyIncluded(const FilePath & filePath) const
	{
		return includedFiles_.count(filePath.ToString()) > 0;
	}

	void IniConfig::Context::LoadFile(const std::string & filePathStr, IniConfig & config)
	{
		SharedPtr<const FilePath> filePath = FindFile(filePathStr);

		if (IsCircularIncluding(*filePath))
		{
			throw Error(FormatString("Circular including of the file '%s'", filePath->ToString()));
		}

		if (WasAlreadyIncluded(*filePath))
		{
			fprintf(stderr, "File '%s' was already included\n", filePath->ToString().c_str());
		}

		LoadFile(filePath, config);
	}

	SharedPtr<const FilePath> IniConfig::Context::CurrentFilePath() const
	{
		if (filePathStack_.empty())
		{
			throw Error("Internal error: attempt to call IniConfig::Context::CurrentFilePath while stack is empty.");
		}

		return filePathStack_.back();
	}

	void IniConfig::Context::PushFilePath(SharedPtr<const FilePath> filePath)
	{
		includedFiles_.insert(filePath->ToString());
		filePathStack_.push_back(filePath);
	}

	void IniConfig::Context::PopFilePath()
	{
		filePathStack_.pop_back();
	}

	void IniConfig::Context::SetVariable(const std::string & key, const Location & location, VariantPtr value)
	{
		const std::map<std::string, LocatedValue>::iterator itr = variables_.find(key);
		if (itr != variables_.end())
		{
			itr->second.location = location;
			itr->second.value = value;
		}
		else
		{
			variables_.insert(std::make_pair(key, LocatedValue(location, value)));
		}
	}

	void IniConfig::Context::AppendVariable(std::deque<char> & result, const std::string & key)
	{
		const std::map<std::string, LocatedValue>::iterator itr = variables_.find(key);
		if (itr != variables_.end())
		{
			const std::string value = itr->second.value.ToString();
			result.insert(result.end(), value.begin(), value.end());
		}
		else
		{
			throw Error(FormatString("Variable '%s' was not defined", key));
		}
	}

	namespace
	{
		static bool ParseKey(const std::string & s, std::string::size_type & i, std::string & key)
		{
			// Assert( i < s.length() && s[i] == '$' )

			++i;
			if (i >= s.length() || s[i] != '{')
			{
				return false;
			}

			++i;
			if (i >= s.length() || !CharClass::IDENTIFIER_HEAD.Contains(s[i]))
			{
				return false;
			}

			const std::string::size_type j = i;
			do
			{
				++i;
			}
			while (i < s.length() && CharClass::IDENTIFIER_TAIL.Contains(s[i]));

			key = s.substr(j, i - j);
			if (i >= s.length() || s[i] != '}')
			{
				return false;
			}

			++i;

			return true;
		}
	}

	void IniConfig::Context::InterpolateString(std::string & s)
	{
		std::string::size_type textBegin = 0;
		std::string::size_type i = s.find_first_of('$');
		if (i == std::string::npos)
		{
			return;
		}

		std::deque<char> result;
		std::string key;

		while (i != std::string::npos)
		{
			const std::string::size_type textEnd = i;
			if (ParseKey(s, i, key))
			{
				if (textBegin < textEnd)
				{
					result.insert(result.end(), s.begin() + textBegin, s.begin() + textEnd);
				}
				textBegin = i;

				AppendVariable(result, key);
			}

			i = s.find_first_of('$', i);
		}

		if (textBegin < s.length())
		{
			result.insert(result.end(), s.begin() + textBegin, s.end());
		}

		s.assign(result.begin(), result.end());
	}

	void IniConfig::Context::LoadFile(SharedPtr<const FilePath> filePath, IniConfig & config)
	{
		if (!filePath->IsAbsolute())
		{
			throw Error("Internal error: not an absolute file path was given to IniConfig::Context::LoadFile method.");
		}

		try
		{
			FileStats fileStats(filePath->ToString(), true);
			if (fileStats.Type() != FileType::REGULAR)
			{
				throw Error(FormatString("File '%s' is not regular or does not exist", filePath->ToString()));
			}

			const FileSize fileSize = fileStats.Size();
			if (fileSize > MAX_INI_CONFIG_SIZE)
			{
				throw Error(FormatString("File '%s' is too big", filePath->ToString()));
			}

			File file(filePath->ToString());
			file.Open(File::OPEN_READ_ONLY);
			FileCloseSentinel fileCloser(file);

			std::vector<char> fileContent(fileSize, '\0');
			if (file.Read(&fileContent[0], fileSize) != fileSize)
			{
				throw Error(FormatString("Read error from file '%s'", filePath->ToString()));
			}

			CharSource::ConstCharPtr charSource(&fileContent[0], fileContent.size());
			PushFilePath(filePath);

			try
			{
				IniConfigParser(charSource, config, *this);
			}
			catch (...)
			{
				PopFilePath();
				throw;
			}

			PopFilePath();
		}
		catch (const UnixError & e)
		{
			throw Error(FormatString("Could not load file '%s': %s", filePath->ToString(), e.What()));
		}
	}

	/********** IniConfig::Section **********/

	IniConfig::Section::Section(const IniConfig & config, const std::string & sectionName)
		: config_(config),
		  sectionName_(sectionName),
		  pairs_()
	{
		;;
	}

	const IniConfig::Section & IniConfig::Section::GetSectionRequired(const std::string & sectionName) const
	{
		return config_.GetSectionRequired(sectionName);
	}

	const IniConfig::Section & IniConfig::Section::GetSectionOptional(const std::string & sectionName) const
	{
		return config_.GetSectionOptional(sectionName);
	}

	bool IniConfig::Section::Contains(const std::string & key) const
	{
		return pairs_.count(key) > 0;
	}

	void IniConfig::Section::Set(const std::string & key, const Location & location, const VariantPtr & value)
	{
		std::map<std::string, LocatedValue>::iterator itr = pairs_.find(key);
		if (itr != pairs_.end())
		{
			if (location.File() == itr->second.location.File())
			{
				fprintf(
					stderr, "Value of [%s/%s] defined in '%s' line %u overrides value in line %u\n",
					sectionName_.c_str(), key.c_str(),
					location.File().c_str(), location.Line(),
					itr->second.location.Line()
				);
			}
			else
			{
				fprintf(
					stderr, "Value of [%s/%s] defined in '%s' line %u overrides value in '%s' line %u\n",
					sectionName_.c_str(), key.c_str(),
					location.File().c_str(), location.Line(),
					itr->second.location.File().c_str(), itr->second.location.Line()
				);
			}

			itr->second.location = location;
			itr->second.value = value;
		}
		else
		{
			pairs_.insert(std::make_pair(key, LocatedValue(location, value)));
		}
	}

	VariantPtr IniConfig::Section::Get(const std::string & key) const
	{
		std::map<std::string, LocatedValue>::const_iterator itr = pairs_.find(key);

		if (itr == pairs_.end())
		{
			throw Error::KeyDoesNotExist(sectionName_, key);
		}

		return itr->second.value;
	}

	const int IniConfig::Section::GetInt(const std::string & key) const
	{
		return GetLongLongInt(key);
	}

	const int IniConfig::Section::GetInt(const std::string & key, int defaultValue) const
	{
		return GetLongLongInt(key, defaultValue);
	}

	const long long int IniConfig::Section::GetLongLongInt(const std::string & key) const
	{
		return Get(key).ToLongLongInt();
	}

	const long long int IniConfig::Section::GetLongLongInt(const std::string & key, const long long int & defaultValue) const
	{
		std::map<std::string, LocatedValue>::iterator itr = pairs_.find(key);
		if (itr == pairs_.end())
		{
			return defaultValue;
		}

		return itr->second.value.ToLongLongInt();
	}

	const double IniConfig::Section::GetDouble(const std::string & key) const
	{
		return Get(key).ToDouble();
	}

	const double IniConfig::Section::GetDouble(const std::string & key, const double & defaultValue) const
	{
		std::map<std::string, LocatedValue>::iterator itr = pairs_.find(key);
		if (itr == pairs_.end())
		{
			return defaultValue;
		}

		return itr->second.value.ToDouble();
	}

	const std::string IniConfig::Section::GetString(const std::string & key) const
	{
		return Get(key).ToString();
	}

	const std::string IniConfig::Section::GetString(const std::string & key, const std::string & defaultValue) const
	{
		std::map<std::string, LocatedValue>::iterator itr = pairs_.find(key);

		if (itr == pairs_.end())
		{
			return defaultValue;
		}

		return itr->second.value.ToString();
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, int & value) const
	{
		value = GetInt(key);
		return *this;
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, int & value, int defaultValue) const
	{
		value = GetInt(key, defaultValue);
		return *this;
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, unsigned int & value) const
	{
		value = GetLongLongInt(key);
		return *this;
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, unsigned int & value, unsigned int defaultValue) const
	{
		value = GetLongLongInt(key, defaultValue);
		return *this;
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, long long int & value) const
	{
		value = GetLongLongInt(key);
		return *this;
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, long long int & value, long long int defaultValue) const
	{
		value = GetLongLongInt(key, defaultValue);
		return *this;
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, double & value) const
	{
		value = GetDouble(key);
		return *this;
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, double & value, double defaultValue) const
	{
		value = GetDouble(key, defaultValue);
		return *this;
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, std::string & value) const
	{
		value = GetString(key);
		return *this;
	}

	const IniConfig::Section & IniConfig::Section::Fetch(const std::string & key, std::string & value, const std::string & defaultValue) const
	{
		value = GetString(key, defaultValue);
		return *this;
	}

	/********** IniConfig **********/

	IniConfig::IniConfig()
		: sections_(),
		  emptySection_(new Section(*this, ""))
	{
		;;
	}

	IniConfig::Section & IniConfig::CreateSection(const std::string & sectionName)
	{
		Section * section = sections_[sectionName];
		if (section == 0)
		{
			std::auto_ptr<Section> newSection(new Section(*this, sectionName));
			section = newSection.get();
			sections_.Set(sectionName, newSection);
		}

		return *section;
	}

	void IniConfig::Set(const std::string & sectionName, const std::string & key, const Location & location, VariantPtr value)
	{
		CreateSection(sectionName).Set(key, location, value);
	}

	const IniConfig::Section & IniConfig::GetSectionRequired(const std::string & sectionName) const
	{
		Section * section = sections_[sectionName];
		if (section == 0)
		{
			throw Error::SectionDoesNotExist(sectionName);
		}

		return *section;
	}

	const IniConfig::Section & IniConfig::GetSectionOptional(const std::string & sectionName) const
	{
		Section * section = sections_[sectionName];

		return section != 0 ? *section : *emptySection_;
	}

	void IniConfig::LoadFromFile(const std::string & filePathStr)
	{
		sections_.Clear();

		FilePath filePath(filePathStr);
		const std::string baseDirPath = filePath.IsAbsolute() ? filePath.Parent().ToString() : FileUtils::GetCurrentDirectory();

		Context(baseDirPath).LoadFile(filePath.ToString(), *this);
	}
}
