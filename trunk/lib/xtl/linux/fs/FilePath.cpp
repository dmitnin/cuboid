#include "FilePath.hpp"

#include "../../Exception.hpp"
#include "FilePathTokenizer.hpp"
#include "FileUtils.hpp"

namespace XTL
{
	class FilePathCreator : public FilePathTokenListener
	{
		public:

			explicit FilePathCreator(FilePath & target)
				: target_(target) { ;; }

			virtual ~FilePathCreator() throw()
			{
				;;
			}

			virtual void OnRootDir()
			{
				target_.SetAbsolute();
			}

			virtual void OnCurrentDir()
			{
				;;
			}

			virtual void OnParentDir()
			{
				target_.AppendParentDir();
			}

			virtual void OnPathPart(const std::string & part)
			{

				target_.AppendPathPart(part);
			}

		private:

			FilePath & target_;
	};

	class FilePathAppender : public FilePathCreator
	{
		public:

			explicit FilePathAppender(FilePath & target)
				: FilePathCreator(target) { ;; }

			virtual ~FilePathAppender() throw()
			{
				;;
			}

			virtual void OnRootDir()
			{
				// Do nothing or throw exception?
			}
	};

	FilePath::FilePath()
		: parts_(),
		  absolute_(false)
	{
		;;
	}

	FilePath::FilePath(const char * filePath)
		: parts_(),
		  absolute_(false)
	{
		Assign(filePath);
	}

	FilePath::FilePath(const std::string & filePath)
		: parts_(),
		  absolute_(false)
	{
		Assign(filePath);
	}

	FilePath::FilePath(const std::string & baseDir, const FilePath & filePath)
		: parts_(),
		  absolute_(false)
	{
		if (filePath.IsAbsolute())
		{
			*this = filePath;
		}
		else
		{
			Assign(baseDir);
			Append(filePath);
		}
	}

	bool FilePath::IsEmpty() const
	{
		return parts_.empty() && !absolute_;
	}

	bool FilePath::IsAbsolute() const
	{
		return absolute_;
	}

	bool FilePath::IsPureParent() const
	{
		return !absolute_ && !parts_.empty() && parts_.back() == "..";
	}

	const std::string FilePath::ToString() const
	{
		if (parts_.empty())
		{
			return absolute_ ? "/" : ".";
		}

		std::string buffer(absolute_ ? "/" : "");

		ListOfParts::const_iterator itr(parts_.begin());
		const ListOfParts::const_iterator end(parts_.end());

		buffer.append(*itr);

		for (++itr; itr != end; ++itr)
		{
			buffer.append("/").append(*itr);
		}

		return buffer;
	}

/*
		for (ListOfParts::const_iterator itr = parts_.begin(); itr != parts_.end(); ++itr)
		{
			fprintf(stderr, "[%s] ", itr->c_str());
		}
		fprintf(stderr, "\n");
*/

	const FilePath FilePath::Parent() const
	{
		FilePath tempFilePath(*this);

		if (tempFilePath.IsPureParent() || tempFilePath.Remove().length() == 0)
		{
			tempFilePath.AppendParentDir();
		}

		return tempFilePath;
	}

	FilePath & FilePath::Assign(const char * filePath)
	{
		parts_.clear();
		absolute_ = false;

		FilePathCreator creator(*this);
		TokenizeFilePath(creator, filePath);

		return *this;
	}

	FilePath & FilePath::Assign(const std::string & filePath)
	{
		return Assign(filePath.c_str());
	}

	FilePath & FilePath::Append(const FilePath & other)
	{
		FilePathAppender appender(*this);
		other.Tokenize(appender);
		return *this;
	}

	FilePath & FilePath::Append(const char * filePath)
	{
		return Append(FilePath(filePath));
	}

	FilePath & FilePath::Append(const std::string & filePath)
	{
		return Append(filePath.c_str());
	}

	const std::string FilePath::Remove()
	{
		if (parts_.empty())
		{
			return std::string();
		}

		const std::string result = parts_.back();
		parts_.pop_back();
		return result;
	}

	FilePath & FilePath::ConvertToAbsolute()
	{
		return ConvertToAbsolute(FileUtils::GetCurrentDirectory());
	}

	FilePath & FilePath::ConvertToAbsolute(const std::string & baseDir)
	{
		return ConvertToAbsolute(FilePath(baseDir));
	}

	FilePath & FilePath::ConvertToAbsolute(const FilePath & basePath)
	{
		if (IsAbsolute())
		{
			return *this;
		}

		if (!basePath.IsAbsolute())
		{
			throw ILLEGAL_ARGUMENT_ERROR("FilePath::ConvertToAbsolute() - basePath is not absolute");
		}

		FilePath tempFilePath(basePath);
		tempFilePath.Append(*this);
		std::swap(parts_, tempFilePath.parts_);

		SetAbsolute();

		return *this;
	}

	void FilePath::SetAbsolute()
	{
		absolute_ = true;
	}

	void FilePath::AppendParentDir()
	{
		if (parts_.empty())
		{
			if (!absolute_)
			{
				parts_.push_back("..");
			}
		}
		else if (parts_.back() == "..")
		{
			parts_.push_back("..");
		}
		else
		{
			parts_.pop_back();
		}
	}

	void FilePath::AppendPathPart(const std::string & part)
	{
		parts_.push_back(part);
	}

	void FilePath::Tokenize(FilePathTokenListener & listener) const
	{
		const ListOfParts::const_iterator end(parts_.end());

		if (IsAbsolute())
		{
			listener.OnRootDir();
		}

		for (ListOfParts::const_iterator itr(parts_.begin()); itr != end; ++itr)
		{
			if (*itr == "..")
			{
				listener.OnParentDir();
			}
			else
			{
				listener.OnPathPart(*itr);
			}
		}
	}

	bool FilePath::operator== (const FilePath & other) const
	{
		return ToString() == other.ToString();
	}

	bool FilePath::operator!= (const FilePath & other) const
	{
		return !operator==(other);
	}

	const FilePath operator+ (const FilePath & left, const FilePath & right)
	{
		return FilePath(left).Append(right);
	}

	const FilePath operator+ (const FilePath & left, const std::string & right)
	{
		return FilePath(left).Append(right);
	}

	const FilePath operator+ (const std::string & left, const FilePath & right)
	{
		return FilePath(left).Append(right);
	}
}

