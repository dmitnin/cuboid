#ifndef XTL__FILE_PATH_HPP__
#define XTL__FILE_PATH_HPP__ 1

#include <string>
#include <vector>

namespace XTL
{
	class FilePathTokenListener;

	class FilePath
	{
		typedef std::vector<std::string> ListOfParts;

		public:

			FilePath();

			explicit FilePath(const char * filePath);

			explicit FilePath(const std::string & filePath);

			FilePath(const std::string & baseDir, const FilePath & filePath);

			bool IsEmpty() const;

			bool IsAbsolute() const;

			/* Check if file path consists of dot-dot parts only like "../../.." */
			bool IsPureParent() const;

			// TODO: Cache value ToString() in the object.
			// Or may be better to change storing format, like string of normalized path plus an array of indices of '/' chars.
			const std::string ToString() const;

			const FilePath Parent() const;

			FilePath & Assign(const char * filePath);

			FilePath & Assign(const std::string & filePath);

			FilePath & Append(const FilePath & other);

			FilePath & Append(const char * filePath);

			FilePath & Append(const std::string & filePath);

			/* TODO: Rename this method. Actually it does pop last part of path and returns it. */
			const std::string Remove();

			FilePath & ConvertToAbsolute();

			FilePath & ConvertToAbsolute(const std::string & baseDir);

			FilePath & ConvertToAbsolute(const FilePath & basePath);

			bool operator== (const FilePath & other) const;

			bool operator!= (const FilePath & other) const;

			class Iterator
			{
				public:

					explicit Iterator(const FilePath & filePath)
						: itr_(filePath.parts_.begin()),
						  end_(filePath.parts_.end()),
						  currentPath_(filePath.IsAbsolute() ? "/" : "")
					{
						if (itr_ != end_)
						{
							currentPath_.append(*itr_);
						}
					}

					bool AtEnd() const
					{
						return itr_ == end_;
					}

					void Advance()
					{
						++itr_;
						if (itr_ != end_)
						{
							currentPath_.append("/").append(*itr_);
						}
					}

					const std::string & CurrentPath() const
					{
						return currentPath_;
					}

				private:

					ListOfParts::const_iterator itr_;
					const ListOfParts::const_iterator end_;
					std::string currentPath_;
			};

		private:

			friend class FilePathCreator;
			friend class FilePathAppender;

			void SetAbsolute();

			void AppendParentDir();

			void AppendPathPart(const std::string & part);

			void Tokenize(FilePathTokenListener & listener) const;

			ListOfParts parts_;
			bool        absolute_;
	};

	const FilePath operator+ (const FilePath & left, const FilePath & right);

	const FilePath operator+ (const FilePath & left, const std::string & right);

	const FilePath operator+ (const std::string & left, const FilePath & right);
}

#endif
